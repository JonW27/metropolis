# metropolis
This is a train stimulation game made as our final project for our APCS class at Stuyvesant.
## Overview
The train stimulation game is based off the game Mini Metro by the New Zealand game studio DinoPoloClub.
## Devlog
- [x] Begin Project
- [ ] Create MVP
- [ ] Add interactivity
- [ ] Create main menu
- [ ] Use different pathfinding algorithms for the people
- [ ] Create population density maps
- [ ] Create trigger factors
- [ ] Create (optional) AI
- [ ] Code coverage
- [ ] Licensing
- [x] README & devlog

Friday June 2nd - did work on Station and People classes
## Features
soon to come.
## Authors
Jonathan Wong and Kevin Lula

