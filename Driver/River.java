public class River{
  double x, y;
  int width, height;
  String type;
  public River(double x, double y, int width, int height, String type){
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.type = type;
  }
  
}
